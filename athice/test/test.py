import athice.corpus as aco
import athice.clustering as acl
import athice.graph as graph

from os import path, getcwd

__location__ = path.realpath(path.join(getcwd(),path.dirname(__file__)))

testfile = path.join(__location__, "test.conllu")
testpos = ['NOUN', 'VERB']
testlbl = ['nsubj', 'obj']

state = { 'analyser' : None , 'n' : 0 , 'sigma' : 0.0 , 'graph' : dict() ,
          'dis_graph' : dict() , 'tree' : dict() }

def init(filename):
    testfile = path.join(__location__, filename)
    print("Initializing test data...")
    analyser = aco.Analyser(testfile)
    analyser.build(testpos,testlbl)
    state['analyser'] = analyser
    print("Ready to process.")

def run_test(n, threshold):
    analyser = state['analyser']
    state['n'] = n
    state['sigma'] = threshold
    print("Clustering predicates...") 
    acl.run_kmean(analyser.predicates, n_clusters=n)
    print("Generating cluster graph...")
    state['graph'] = acl.make_graph(analyser, threshold=threshold)
    return state['graph']

def full_test(filename="test.conllu", n_clusters=10, threshold=0.85,
              heuristics = graph.most_critical):
    init(filename)
    g = run_test(n_clusters, threshold)
    state['dis_graph'] = graph.disambiguate(g, heuristics=heuristics)
    print('Converting graph to tree...')
    state['tree'] = graph.treeify(state['dis_graph'])
    print(f"Details of the test written in [state] dictionary")

def write_last_graph(filename="test.dot"):
    graph.write_dot(filename, state['dis_graph'])

def write_last_tree(filename="test.dot"):
    graph.write_dot(filename, state['tree'])
