import grew

_unwanted_features = ["Number", "xpos", "form", "textform", "wordform",
    "Mood", "Tense", "VerbForm", "SpaceAfter" , "SpacesAfter"  , "Person" ] 

class Analyser:
    """
    Gather all the data relative to the analysis of a corpus.

    Attributes:
       predicates — stores data for predicates
       arguments — stores data for arguments
    """

    class _Part:
       """
       Generic class for handling corpus related data.

       Attributes:
          dict — a dictionary mapping words to their respective neighborhoods
          clusters — a clustering a the words in dict.keys()
       """
       def __init__(self):
           self.dict = dict()
           self.clusters = dict()

       def as_prevector(self):
           """
           Output the word-neighborhood mapping in a form acceptable by sklearn
           feature extraction functions.
           """
           return [{'to': l} for l in self.dict.values()]

       def as_list(self):
           """
           Output the list of words in dict.keys().
           """
           return list(self.dict.keys())

    def __init__(self, filename):
        """
        Load a corpus file.

        Parameters:
           filename — name of a corpus file in ConLL format.
        """
        grew.init()
        self._index = grew.corpus(filename)
        self.predicates = self._Part()
        self.arguments = self._Part()

    def build(self, uposs, labels):
        """
        Extract relevant dependency relations from the corpus and generate
        neighborhood dictionaries for predicates and arguments. Update class
        attributes in place.

        Parameters:
           uposs — list of strings representing upos values to keep
           labels — list of strings representing edge labels to keep
        """
        r = """        rule r1 {{
           pattern {{ N [upos <> {juposs}] }} commands {{ del_node N }}
        }}
        rule r2 {{
          pattern {{ e:N -[^{jlbls}]-> M }} commands {{ del_edge e }}
        }}
        rule r3 {{
           pattern {{ N [] }} without {{ N -> * }} without {{ * -> N }}
           commands {{ del_node N }}
        }}
        """.format(juposs="|".join(uposs),jlbls="|".join(labels))
        f = """rule f{num} {{
           pattern {{ N [{feat}] }} commands {{ del_feat N.{feat} }}
        }}
        """
        for i, ft in enumerate(_unwanted_features):
            r += f.format(num=i+1, feat=ft)
        r += """
        strat reduce { Seq(Iter(Pick(r1)), Iter(Pick(r2)), Iter(Pick(r3))) }
        strat erase { Seq("""
        r += ", ".join(
            ["Iter(Pick(f{}))".format(i)
             for i in range(1,len(_unwanted_features))]
        ) + """) }
        strat main { Seq(reduce, erase) }"""
        rs = grew.grs(r)
        l = [ grew.run(rs, grew.corpus_get(i, self._index))
              for i in range(1, grew.corpus_size(self._index)) ]
        preds = dict()
        args = dict()
        for g in l:
            d = g[0]
            for k in d.keys():
                out_edges = d[k][1]
                if len(out_edges) != 0:
                    for e in out_edges:
                        src = d[k][0]['lemma']+'_'+d[k][0]['upos'][0].lower()
                        src += '_' + e[0]
                        nd = e[1]
                        trg = d[nd][0]['lemma']+'_'+d[nd][0]['upos'][0].lower()
                        preds[src] = preds.setdefault(src, []) + [trg]
                        args[trg] = args.setdefault(trg, []) + [src]
        self.predicates.dict = preds
        self.arguments.dict = args
