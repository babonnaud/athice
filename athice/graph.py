import grew

def _to_grew(graph):
    """
    Parameters:
       graph — a cluster graph

    Returns:
       ggraph — a graph usable by grew
    """
    ggraph = dict()
    for k in graph.keys():
        ggraph[k] = [{'label': k}, [['', n] for n in graph[k]]]
    return ggraph

def _leaf_of(node, graph):
    """
    Parameters:
       node — the string name of a node
       graph — a cluster graph

    Returns:
       leaf — the name of the leaf of [node] if it exists, and None otherwise
    """
    res = []
    for n in graph[node]:
        if _parents_of(n, graph) == [node] and graph[n] == []:
            res.append(n)
    assert len(res) < 2
    return res[0] if len(res) == 1 else None

def _parents_of(node, graph):
    """
    Parameters:
       node — the string name of a node
       graph — a cluster graph

    Returns:
       parents — a list containing the names of the parents of [node]
    """
    return [n for n in graph.keys() if node in graph[n]]

def _descendants_of(node, graph):
    """
    Parameters:
       node — the string name of a node
       graph — a cluster graph

    Returns:
       descendants — a set containing the names of the descendants of [node]
    """
    res = set(graph[node])
    for n in graph[node]:
        res = res.union(_descendants_of(n, graph))
    return res

def _parent_clusters(node, graph):
    """
    Parameters:
       node — the string name of a node
       ggraph — a cluster graph

    Returns:
       clusters — a list of list of parent nodes of [node]
    """
    parents = _parents_of(node, graph)
    parents.sort(key=lambda p: len(set(graph[p]).intersection(set(parents))))
    clusters = []
    while len(parents) != 0:
       source = parents[0]
       parents.remove(source)
       cluster = [source]
       for p in _parents_of(source, graph):
           if p in parents:
               cluster.append(p)
               parents.remove(p)
       clusters.append(cluster)
    return clusters

def _safe_copy(graph):
    """
    Parameters:
       graph —  a cluster graph

    Returns:
       new_graph — a copy of the input graph
    """
    new_graph = dict()
    for k in graph.keys():
        new_graph[k] = graph[k].copy()
    return new_graph

def write_dot(filename, graph):
    """
    Graph printing utility.

    Parameters:
       filename — name of a file to write
       graph — a cluster graph

    Returns:
       None
    """
    text = "digraph G{\n"
    table = str.maketrans('(+)','___')
    for n in graph.keys():
        text += "{} [label=\"{}\"]\n".format(n.translate(table), n)
    for n in graph.keys():
        for t in graph[n]:
            text += "{}->{}\n".format(n.translate(table), t.translate(table))
    text += "}"
    with open(filename, 'w') as f:
        _  = f.write(text)

def p4(graph):
    """
    Look for unwanted patterns in the input graph.

    Parameters:
       graph — a cluster graph

    Returns:
       p4s — a list of all the p4 patterns in [graph]
    """
    ggraph = _to_grew(graph)
    p4s = []
    pat = "pattern {{ X[] ; Y[] ; Z[] ; K[] ; {} }} "
    pat += "without {{ X -> Z }} without {{ Z -> X }}"
    pat += "without {{ Y -> K }} without {{ K -> Y }}"
    try:
        p4s += grew.search(pat.format('X -> Y ; Y -> Z ; Z -> K'), ggraph)
    except grew.utils.GrewError:
        pass
    try:
        p4s += grew.search(pat.format('X -> Y ; Y -> Z ; K -> Z'), ggraph)
    except grew.utils.GrewError:
        pass
    try:
        p4s += grew.search(pat.format('X -> Y ; Z -> Y ; Z -> K'), ggraph)
    except grew.utils.GrewError:
        pass
    try:
        p4s += grew.search(pat.format('Y -> X ; Y -> Z ; Z -> K'), ggraph)
    except grew.utils.GrewError:
        pass
    return [tuple([p['nodes'][x] for x in ['X','Y','Z','K']]) for p in p4s]

def split(node, graph):
    """
    Split [node] in the input graph. The input graph is not modified.

    Parameters:
       node — the string name of a node
       graph — a cluster graph

    Returns:
       new_graph — a cluster graph
    """
    print(f'this is split, called on {node}')
    new_graph = _safe_copy(graph)
    clusters = _parent_clusters(node, new_graph)
    args = set(new_graph[node])
    if args == set(): # Argument case
        for i, c_i in enumerate(clusters, start=1):
            print(f'. arg case {i}, parent cluster is {c_i}')
            if '+' in node:                # create new node
                n_node = '(' + node + ')' + f'_{i}'
            else:
                n_node = node + f'_{i}'
            if len(c_i) == 1:              # if single parent, check leaf
                parent = c_i[0]
                leaf = _leaf_of(parent, new_graph)
                if leaf is not None:       # then merge new node with leaf
                    n_node = leaf + '+' + n_node
                    new_graph[parent].remove(leaf)
                    _ = new_graph.pop(leaf)
            for parent in c_i:             # in any case, add n_node
                new_graph[parent].append(n_node)
            new_graph[n_node] = []
    else: # Predicate case
        for i, c_i in enumerate(clusters, start=1):
            if '+' in node:                    # create new node
                n_node = '(' + node + ')' + f'_{i}'
            else:
                n_node = node + f'_{i}'
            children_i = set(new_graph[node])  # compute children
            for parent in c_i:
                children_i = children_i.intersection(set(new_graph[parent]))
            # check whether a child is a predicate isomorphic to n_node ;
            # while it is the case, nodes must me merged.
            hc = max(children_i, key=lambda n: len(new_graph[n]))
            same_chld = children_i.difference({hc}) == set(new_graph[hc])
            prnt_hc = set(_parents_of(hc, new_graph))
            same_prnt = prnt_hc.difference({node}) == set(c_i)
            while same_chld and same_prnt:
                n_node = hc + '+' + n_node
                children_i.remove(hc)
                for parent in c_i:
                    new_graph[parent].remove(hc)
                _ = new_graph.pop(hc)
                hc = max(children_i, key=lambda n: len(new_graph[n]))
                same_chld = children_i.difference({hc}) == set(new_graph[hc])
                prnt_hc = set(_parents_of(hc, new_graph))
                same_prnt = prnt_hc.difference({node}) == set(c_i)
            if len(c_i) == 1:   # if single parent, check isomorphism as well
                parent = c_i[0]
                if set(new_graph[parent]) == children_i.union({node}):
                    # isomorphism is only possible if cluster node has no
                    # parent itself.
                    if len(_parents_of(parent, new_graph)) == 0:
                        n_node = parent + '+' + n_node
                        _ = new_graph.pop(parent)
                        c_i.remove(parent)     # remove it for next line
            for parent in c_i:                 # in any case, add n_node
                new_graph[parent].append(n_node)
            new_graph[n_node] = list(children_i)
            args.difference_update(children_i) # remove treated children
        # if children are remaining, we introduce new nodes for them.
        leaf = _leaf_of(node, new_graph)           # special case for leaf
        if leaf is not None:
            if '+' in node:                # create new node
                n_node = '(' + node + ')_0'
            else:
                n_node = node + '_0'
            new_graph[n_node] = [leaf]
            args.remove(leaf)
        args = list(args)
        args.sort(key=lambda n: len(new_graph[n]), reverse=True)
        treated_args = set()
        for i, remaining_arg in enumerate(args, start=len(clusters) + 1):
            if remaining_arg not in treated_args:
                children = set(new_graph[remaining_arg]).union({remaining_arg})
                children.intersection_update(args)
                preds = [n for n in children if len(new_graph[n]) != 0]
                if '+' in node:                # create new node
                    n_node = '(' + node + ')' + f'_{i}'
                else:
                    n_node = node + f'_{i}'
                # at this point, we also have to check isomorphism with children
                # and merge nodes if necessary
                if remaining_arg in preds:
                    hc = remaining_arg
                    same_chld = children.difference({hc}) == set(new_graph[hc])
                    same_prnt = set(_parents_of(hc, new_graph)) == {node}
                    while len(preds) != 0 and same_chld and same_prnt:
                        n_node = hc + '+' + n_node
                        preds.remove(hc)
                        children.remove(hc)
                        _ = new_graph.pop(hc)
                        if len(preds) != 0:
                            hc = max(preds, key=lambda n: len(new_graph[n]))
                            same_chld = children.difference({hc}) ==\
                                set(new_graph[hc])
                            same_prnt = set(_parents_of(hc, new_graph)) ==\
                                {node}
                new_graph[n_node] = list(children)
                for n in children: treated_args.add(n)
    # Delete initial node
    for p in _parents_of(node, new_graph): new_graph[p].remove(node)
    _ = new_graph.pop(node)
    return new_graph

def most_critical_args_only(graph):
    """
    Select the next node to be split by identifying the argument node whose
    splitting will remove the greatest number of P4 patterns.

    Parameters:
       graph — a cluster graph

    Returns:
       node — an argument node in the graph
    """
    criticality = dict()
    for t in p4(graph):
        if t[1][0] == 'A':
            criticality[t[1]] = criticality.setdefault(t[1], 0) + 1
        if t[2][0] == 'A':
            criticality[t[2]] = criticality.setdefault(t[2], 0) + 1
    n = max(criticality.keys(), key=lambda nd: criticality[nd])
    return n

def most_critical(graph):
    """
    Select the next node to be split by identifying the node whose splitting
    will remove the greatest number of P4 patterns.

    Parameters:
       graph — a cluster graph

    Returns:
       node — an argument node in the graph
    """
    criticality = dict()
    for t in p4(graph):
        criticality[t[1]] = criticality.setdefault(t[1], 0) + 1
        criticality[t[2]] = criticality.setdefault(t[2], 0) + 1
    n = max(criticality.keys(), key=lambda nd: criticality[nd])
    return n

def disambiguate(graph, heuristics=most_critical_args_only):
    """
    Disambiguate the graph following a given heuristics. The input graph is
    not modified.

    Parameters:
       graph — a cluster graph
       heuristics — a function which inputs a graph and returns a node

    Returns:
       dis_graph — a disambiguated graph
    """
    dis_graph = _safe_copy(graph)
    print("Starting disambiguation. This may take some time...")
    i = 0
    while len(p4(dis_graph)) != 0:
       next_node = heuristics(dis_graph)
       dis_graph = split(next_node, dis_graph)
       i += 1
    print(f"Disambiguation completed in {i} steps.")
    return dis_graph

def _merge(graph):
    """
    Merge isomorphic nodes. Input graph is not modified.

    Parameters:
       graph — a cluster graph

    Returns:
       new_graph — a cluster graph
    """
    new_graph = _safe_copy(graph)
    isomorphic = lambda n1, n2: set(new_graph[n1]) == set(new_graph[n2])\
       and set(_parents_of(n1, new_graph)) == set(_parents_of(n2, new_graph))
    nodes = list(new_graph.keys())
    while len(nodes) != 0:
        n = nodes.pop()
        isos = [m for m in nodes if isomorphic(n,m)]
        if len(isos) > 0:
            n_node = n + '+' + '+'.join(isos)
            new_graph[n_node] = new_graph[n].copy()
            for p in _parents_of(n, new_graph):
                for m in isos: new_graph[p].remove(m)
                new_graph[p].remove(n)
                new_graph[p].append(n_node)
            for m in isos:
                _ = new_graph.pop(m)
                nodes.remove(m)
            _ = new_graph.pop(n)
    return new_graph

def _compact(graph):
    """
    Merge leaves with their parents if it is their only child.

    Parameters:
       graph — a cluster graph

    Returns:
       new_graph — a cluster graph
    """
    new_graph = _safe_copy(graph)
    is_trans = lambda n: len(new_graph[n]) == 1\
        and len(_parents_of(n, new_graph)) == 1\
        and set(new_graph[n]) ==\
            set(new_graph[_parents_of(n, new_graph)[0]]).difference({n})\
        and len(_parents_of(_parents_of(n, new_graph)[0], new_graph)) == 0
    trans = [n for n in new_graph.keys() if is_trans(n)]
    while len(trans) != 0:
        node = trans.pop()
        parent = _parents_of(node, new_graph)[0]
        n_node = parent + '+' + node
        new_graph[n_node] = new_graph[node].copy()
        _ = new_graph.pop(node) 
        _ = new_graph.pop(parent) 
        trans = [n for n in new_graph.keys() if is_trans(n)]
    args = [n for n in new_graph.keys() if len(new_graph[n]) == 0]
    for node in args:
        prt = _parents_of(node, new_graph)
        assert len(prt) == 1
        parent = prt[0]
        if new_graph[parent] == [node]:
            n_node = parent + '+' + node
            new_graph[n_node] = []
            for p in _parents_of(parent, new_graph):
                new_graph[p].remove(parent)
                new_graph[p].append(n_node)
            _ = new_graph.pop(parent)
            _ = new_graph.pop(node)
    return new_graph

def treeify(graph):
    """
    Turn a disambiguated graph to a hierarchical tree. Warning: the output on
    non-disambiguated graphs is unspecified.

    Parameters:
       graph — a (disambiguated) cluster graph

    Returns:
       tree — a hierarchical cluster tree
    """
    new_graph = _merge(graph)
    new_graph = _compact(new_graph)
    nodes = list(new_graph.keys())
    relatives = lambda n:set(new_graph[n]).union(set(_parents_of(n,new_graph)))
    size = lambda n: len(relatives(n))
    nodes.sort(key=size)
    tree = dict()
    while len(nodes) != 0:
        node = nodes.pop()
        sn = size(node)
        if len(relatives(node)) != 0:
            assert size(max(relatives(node), key=size)) < sn
            visited = {node}
            tree[node] = []
            remaining = relatives(node)
            for i in range(sn-1,0,-1):
                stage = [n for n in remaining if size(n) == i]
                for n in stage:
                    tree[n] = []
                    pn = min([m for m in visited if m in relatives(n)], key=size)
                    tree[pn].append(n)
                visited.update(set(stage))
            for n in visited: nodes.remove(n)
        else:
            tree[node] = []
    tops = [n for n in tree.keys() if len(_parents_of(n, tree)) == 0]
    if len(tops) > 1:
        tree['E'] = tops
    return tree
