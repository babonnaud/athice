from sklearn.feature_extraction import DictVectorizer
import sklearn.cluster as kl

def _convert(precls, init_dict):
    """
    Convert abstract cluster predictions to human-readable form.

    Parameters:
       precls — an array output of a predict() function
       init_dict — a dictionnary containing the initial data for the
          prediction

    Returns:
       cls — a dictionnay assigning to each cluster number the list of its
          content.
    """
    cls = dict()
    for i in range(0,precls.max()+1):
        for elt in range(0,len(init_dict)):
            if precls[elt] == i:
                if i in cls.keys():
                    cls[i] += [list(init_dict.keys())[elt]]
                else:
                    cls[i] = [list(init_dict.keys())[elt]]
    return cls

def run_kmean(part, **kwargs):
    """
    Run a k-mean clustering algorithm. Intended for use on attributes of
    the class athice.corpus.Analyser. Update the input in place.

    Parameters:
       part — an object with word-neighborhood information
       **kwargs — arguments to be passed to sklearn.cluster.KMeans()
    """
    v = DictVectorizer()
    data = v.fit_transform(part.as_prevector()).toarray()
    k = kl.KMeans(**kwargs)
    precls = k.fit_predict(data)
    part.clusters = _convert(precls, part.dict)

def _span_of(analyser, pred_cluster):
    """
    Compute the span of a predicate cluster.

    Parameters:
       analyser — an athice.corpus.Analyser object
       pred_cluster — an integer

    Returns:
       span — the set of arguments spanned by elements of pred_cluster
    """
    span = set()
    for w in analyser.predicates.clusters[pred_cluster]:
        span = span.union(set(analyser.predicates.dict[w]))
    return span

def make_graph(analyser, threshold=0.85):
    """
    Generate a graph of clusters: the predicates clusters are first put into
    the graph, then prehierarchisation is perfomed on them according to their
    spans. Meanwhile, argument clusters are computed on the basis of span
    intersections, and both the graph and the analyser are updated accordingly.

    Parameters:
       analyser — an athice.corpus.Analyser object
       threshold — a decision threshold for prehierarchisation
    
    Returns:
       graph — a cluster graph
    """
    graph = dict()
    P = 'P{}'.format
    A = 'A{}'.format
    args = analyser.arguments.dict.keys()
    args_clustering = dict()
    for w in args:
        args_clustering[w] = 0
    for i in analyser.predicates.clusters.keys():
        graph[P(i)] = []
        si = _span_of(analyser, i)
        for w in si:
            args_clustering[w] += 2**i
        for j in analyser.predicates.clusters.keys():
            if i != j:
                sj = _span_of(analyser, j)
                if len(sj.intersection(si))/len(sj) >= threshold:
                    graph[P(i)].append(P(j))
                    for w in sj.difference(si):
                        args_clustering[w] += 2**i # May have to be removed
    for idx, val in enumerate(set(args_clustering.values())):
        analyser.arguments.clusters[idx] = []
        for w in args:
            if args_clustering[w] == val:
                analyser.arguments.clusters[idx].append(w)
        graph[A(idx)] = []
        # Next line computes exposants in the binary representation of 'val' 
        parents = [i for i,x in enumerate(bin(val)[::-1]) if x == '1']
        for i in parents:
            graph[P(i)].append(A(idx))
    return graph
