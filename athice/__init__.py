def __version__():
   name = "Acquisition of Type Hierarchies through Corpus Exploration"
   version = "v.0.1"
   print(name, version)
