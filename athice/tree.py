from textwrap import fill

def explain(tree, analyser):
    """
    Build types from tree data.

    Parameters:
       tree — a hierarchical cluster tree
       analyser — an athice.corpus.Analyser objet

    Returns:
       explanation — a dictionary assigning list of words to tree nodes
    """
    explanation = dict()
    for node in tree.keys():
        components = node.split('+')
        components = [s.replace('(','').replace(')','') for s in components]
        components = [s.split('_')[0] for s in components]
        words = []
        for s in components:
            if s[0] == 'A':
                words += analyser.arguments.clusters[int(s[1:])]
            elif s[0] == 'P':
                words += analyser.predicates.clusters[int(s[1:])]
        explanation[node] = words
    return explanation

def export(expl, filename='test.data'):
    """
    Parameters:
        expl — a dictionary assigning list of words to node names
        filename — a file name

    Returns:
        None 
    """
    text = ''
    for k, v in expl.items():
        v = [w.replace('_','\\_') for w in v]
        text += f'\expandafter\def\csname type{k}\endcsname\n'
        text += '{{{}}}\n'.format(fill(', '.join(v),width=76))
    with open(filename, 'w') as f:
        _ = f.write(text)
